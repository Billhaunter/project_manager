const express= require('express');

function create (req, res, next) {
    res.send('Skills create');
}

function list (req, res, next) {
    res.send('Skills list');
  }

function index (req, res, next) {
    res.send('Skills index');
}

function replace (req, res, next) {
    res.send('Skills replace');
}

function update(req, res, next) {
    res.send('Skills update');
}

function destroy (req, res, next) {
    res.send('Skills destroy');
}

module.exports={
    create, list, index, replace, update, destroy
};