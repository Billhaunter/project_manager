const express= require('express');

function create (req, res, next) {
    res.send('Sprint Backlogs create');
}

function list (req, res, next) {
    res.send('Sprint Backlogs list');
  }

function index (req, res, next) {
    res.send('Sprint Backlogs index');
}

function replace (req, res, next) {
    res.send('Sprint Backlogs replace');
}

function update(req, res, next) {
    res.send('Sprint Backlogs update');
}

function destroy (req, res, next) {
    res.send('Sprint Backlogs destroy');
}

module.exports={
    create, list, index, replace, update, destroy
};