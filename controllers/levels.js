const express= require('express');

function create (req, res, next) {
    res.send('Levels create');
}

function list (req, res, next) {
    res.send('Levels list');
  }

function index (req, res, next) {
    res.send('Levels index');
}

function replace (req, res, next) {
    res.send('Levels replace');
}

function update(req, res, next) {
    res.send('Levels update');
}

function destroy (req, res, next) {
    res.send('Levels destroy');
}

module.exports={
    create, list, index, replace, update, destroy
};