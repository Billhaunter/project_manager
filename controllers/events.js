const express= require('express');

function create (req, res, next) {
    res.send('Events create');
}

function list (req, res, next) {
    res.send('Events list');
  }

function index (req, res, next) {
    res.send('Events index');
}

function replace (req, res, next) {
    res.send('Events replace');
}

function update(req, res, next) {
    res.send('Events update');
}

function destroy (req, res, next) {
    res.send('Events destroy');
}

module.exports={
    create, list, index, replace, update, destroy
};