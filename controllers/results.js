const express= require('express');

function create (req, res, next) {
    res.send('Results create');
}

function list (req, res, next) {
    res.send('Results list');
  }

function index (req, res, next) {
    res.send('Results index');
}

function replace (req, res, next) {
    res.send('Results replace');
}

function update(req, res, next) {
    res.send('Results update');
}

function destroy (req, res, next) {
    res.send('Results destroy');
}

module.exports={
    create, list, index, replace, update, destroy
};