const express= require('express');

function create (req, res, next) {
    res.send('Logins create');
}

function list (req, res, next) {
    res.send('Logins list');
  }

function index (req, res, next) {
    res.send('Logins index');
}

function replace (req, res, next) {
    res.send('Logins replace');
}

function update(req, res, next) {
    res.send('Logins update');
}

function destroy (req, res, next) {
    res.send('Logins destroy');
}

module.exports={
    create, list, index, replace, update, destroy
};